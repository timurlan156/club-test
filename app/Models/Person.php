<?php


namespace App\Models;


class Person
{
    private $name;
    private $preferredGenre;
    
    public function __construct($name = null, MusicGenre $preferredGenre = null)
    {
        $this->name           = $name;
        $this->preferredGenre = $preferredGenre;
    }
    
    /**
     * Назначить имя посетителю клуба
     *
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * Получить имя посетителя
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Назначить предпочитаемый жанр посетителю клуба
     *
     * @param $preferredGenre
     */
    public function setPreferredGenre(MusicGenre $preferredGenre)
    {
        $this->preferredGenre = $preferredGenre;
    }
    
    /**
     * Получить предпочитаемый жанр посетителя
     */
    public function getPreferredGenre()
    {
        return $this->preferredGenre;
    }
}