<?php

namespace App\Models;

use App\Models\MusicGenre;

class Song
{
    // Длительность трека по умолчанию в секундах
    const DEFAULT_DURATION = 10;
    
    // Объект класса App\Models\MusicGenre
    private $genre;
    
    // Название трека
    private $name;
    
    // Длительность трека
    private $duration;
    
    public function __construct($name = null, MusicGenre $genre = null, $duration = self::DEFAULT_DURATION)
    {
        $this->name  = $name;
        $this->genre = $genre;
        $this->duration = $duration;
    }
    
    /**
     * Назначить имя трека
     *
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * Получить название трека
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Назначить жанр
     *
     * @param MusicGenre $genre
     */
    public function setGenre(MusicGenre $genre)
    {
        $this->genre = $genre;
    }
    
    /**
     * Получить жанр
     */
    public function getGenre()
    {
        return $this->genre;
    }
    
    /**
     * Назначить длительность проигрывания трека
     *
     * @param $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }
    
    /**
     * Получить длительность проигрывания трека
     */
    public function getDuration()
    {
        return $this->duration ?: self::DEFAULT_DURATION;
    }
}