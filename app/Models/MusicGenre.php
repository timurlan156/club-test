<?php


namespace App\Models;


class MusicGenre
{
    private $name;
    
    private $danceBehavior;
    
    public function __construct($name = null, $danceBehavior = null)
    {
        $this->name          = $name;
        $this->danceBehavior = $danceBehavior;
    }
    
    /**
     * Назначить название жанру (RnB, Electro, Pop и т.п.)
     *
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * Получить название жанра
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Назначить поведение танцующего в данном жанре
     * Пр: Двигает головой вниз-вверх и качает правой рукой
     *
     * @param string $behavior
     */
    public function setDanceBehavior($behavior)
    {
        $this->danceBehavior = $behavior;
    }
    
    /**
     * Получить поведение танцующего в данном жанре
     */
    public function getDanceBehavior()
    {
        return $this->danceBehavior;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
}