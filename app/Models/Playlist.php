<?php


namespace App\Models;


class Playlist
{
    private $songs;
    private $currentSongId;
    private $currentSong;
    private $startedPlayingTime;
    
    public function __construct(array $songs)
    {
        $this->songs = $songs;
    }
    
    /**
     * Загрузить треки плейлиста
     *
     * @param array $songs
     */
    public function setSongs(array $songs)
    {
        $this->songs = $songs;
    }
    
    /**
     * Получить треки плейлиста
     */
    public function getSongs()
    {
        return $this->songs;
    }
    
    /**
     * Получить трек, играющий в данный момент
     */
    public function getCurrentSong()
    {
        return $this->currentSong;
    }
    
    /**
     * Назначить трек, играющий в данный момент
     * @param Song|null $song
     * @return Song
     */
    public function setCurrentSong(Song $song = null)
    {
        return $this->currentSong = $song;
    }
    
    /**
     * Продолжить играть треки
     */
    public function resumePlaying()
    {
        $this->startedPlayingTime = $_SESSION['started_playing_time'];
        
        $this->currentSongId = $this->getCurrentSongIdByTime();
        
        if ($this->currentSongId !== null) {
            $this->currentSong = $this->getSongs()[$this->currentSongId];
        } else {
            $this->currentSong = null;
        }
    }
    
    /**
     * Начать проигрывать плейлист
     */
    public function beginPlaying()
    {
        $this->currentSongId = 0;
        $this->currentSong   = $this->getSongs()[0];
    
        $this->startedPlayingTime = intval(microtime(true));
        $_SESSION['started_playing_time'] = $this->startedPlayingTime;
    }
    
    /**
     * Получить индекс играющейго сейчас трека
     *
     * @return int|string
     */
    public function getCurrentSongIdByTime()
    {
        $playlistProgress = $this->getProgress();
        $loopDuration     = 0;
        
        foreach ($this->songs as $key => $song) {
            $loopDuration += $song->getDuration();
            
            if ($playlistProgress <= $loopDuration) {
                return $key;
            }
        }
        
        return null;
    }
    
    /**
     * Получить время проигрывания текущего трека
     */
    public function getProgress()
    {
        return intval(microtime(true)) - $this->startedPlayingTime;
    }
    
}