<?php

namespace App\Services;

use App\Models\Song;

class Club
{
    // Типичное поведение нетанцующего во всех клубах
    const NO_DANCE_BEHAVIOR = 'пьет водку';
    
    private $persons;
    
    private $dj;
    
    public function __construct(Dj $dj = null, array $persons = null)
    {
        $this->dj      = $_SESSION['dj'] ?? $dj;
        $this->persons = $_SESSION['persons'] ?? $persons;
        
        if (! isset($_SESSION['dj']) || empty($_SESSION['dj'])) {
            $_SESSION['dj'] = $this->dj;
        }
        
        if (! isset($_SESSION['persons']) || empty($_SESSION['persons'])) {
            $_SESSION['persons'] = $this->persons;
        }
    }
    
    /**
     * Назначить (пригласть) DJ
     * @param Dj $dj
     */
    public function inviteDJ(Dj $dj) // setDj: ok
    {
        $this->dj = $dj;
    }
    
    /**
     * Начать тусовку
     */
    public function initParty()
    {
        if ($this->dj && $this->dj->isReady()) {
            $this->dj->play();
        }
        
        $this->printActivity();
    }
    
    /**
     * Закончить тусовку
     */
    public function endParty()
    {
        $this->dj->stop();
        
        $this->printActivity();
        
        unset($_SESSION['dj']);
        unset($_SESSION['persons']);
    }
    
    /**
     * Вывести обстановку клуба
     */
    private function printActivity()
    {
        if ($this->dj && $this->dj->isPlaying()) {
            $currentSong = $this->dj->getPlaylist()->getCurrentSong();
            
            if ($currentSong) {
                $this->printMusicInfo($currentSong);
                
                foreach ($this->persons as $person) {
                    if ($person->getPreferredGenre()->getName() === $currentSong->getGenre()->getName()) {
                        echo $person->getName()
                            . ' '
                            . mb_convert_case($person->getPreferredGenre()->getDanceBehavior(), MB_CASE_LOWER)
                            . '<br><br>';
                    } else {
                        echo $person->getName() . ' ' . self::NO_DANCE_BEHAVIOR . '<br><br>';
                    }
                }
            } else {
                $this->printMusicInfo();
                self::printAllPeopleDrinking($this->persons);
            }
        } else {
            $this->printMusicInfo();
            
            self::printAllPeopleDrinking($this->persons);
        }
    }
    
    /**
     * Вывести всех посетителей пьющими
     * @param $persons
     */
    private function printAllPeopleDrinking($persons)
    {
        foreach ($persons as $person) {
            echo $person->getName() . ' ' . self::NO_DANCE_BEHAVIOR . '<br><br>';
        }
    }
    
    /**
     * Вывести информацию о играющем треке
     * @param Song $song
     */
    private function printMusicInfo(Song $song = null)
    {
        if ($song) {
            echo '<b>Сейчас играет трек:</b> ' . $song->getName() . '<br>';
            echo '<b>Жанр трека:</b> ' . $song->getGenre() . '<br><br>';
            echo '<b>Текущий прогресс плейлиста (сек):</b> ' . $this->dj->getPlaylist()->getProgress() . '<br><br>';
        } else {
            echo '<b>DJ перестал играть:</b><br><br>';
        }
    }
}