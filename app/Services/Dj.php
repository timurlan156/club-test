<?php


namespace App\Services;


use App\Models\Playlist;

class Dj
{
    private $isPlaying;
    private $playlist;
    
    public function __construct(Playlist $playlist = null)
    {
        $this->isPlaying = $_SESSION['is_playing'] ?? false;
        $this->playlist  = $_SESSION['playlist'] ?? $playlist;
        
        if (! isset($_SESSION['playlist']) || empty($_SESSION['playlist'])) {
            $_SESSION['playlist'] = $this->playlist;
        }
    }
    
    /**
     * Включить / продолжить музыку
     */
    public function play()
    {
        if ($this->isPlaying) {
            $this->playlist->resumePlaying();
        } else {
            $this->playlist->beginPlaying();
            
            $this->isPlaying = true;
            
            $_SESSION['is_playing'] = $this->isPlaying;
        }
    }
    
    /**
     * Проверить есть ли все для DJ
     */
    public function isReady()
    {
        return ! empty($this->playlist);
    }
    
    /**
     * Проверить играет ли уже DJ
     */
    public function isPlaying()
    {
        return $this->isPlaying;
    }
    
    /**
     * Загрузить плейлист
     * @param Playlist $playlist
     */
    public function setPlaylist(Playlist $playlist)
    {
        $this->playlist = $playlist;
    }
    
    /**
     * Получить плейлист
     */
    public function getPlaylist()
    {
        return $this->playlist;
    }
    
    /**
     * Выключить музыку
     */
    public function stop()
    {
        $this->isPlaying = false;
        $this->playlist->setCurrentSong(null);
        
        unset($_SESSION['is_playing']);
        unset($_SESSION['playlist']);
        unset($_SESSION['started_playing_time']);
    }
}