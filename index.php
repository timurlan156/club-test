<?php

use App\Models\MusicGenre;
use App\Models\Person;
use App\Models\Playlist;
use App\Models\Song;
use App\Services\Club;
use App\Services\Dj;

require 'bootstrap.php';

// Набор жанров (3 шт. для примера)

$rapGenre     = new MusicGenre('Rap', 'Держится за штанину левой рукой, и качает правой');
$rnbGenre     = new MusicGenre('RnB', 'Движется в ритме спокойных битов, подняв локти вверх');
$electroGenre = new MusicGenre('Electro', 'Взрывает танцпол под стробосками, закрыв глаза');

// Набор треков под каждый из жанров (также, с указанием длительности в секундах)

$rapSong     = new Song('Desiigner - Outlet', $rapGenre, 15);
$rnbSong     = new Song('The Weeknd - Escape From LA', $rnbGenre);
$electroSong = new Song('Don Diablo - We Are Love', $electroGenre, 5);

// Плейлист, в который включаем те или иные треки

$playlist = new Playlist([$rapSong, $rnbSong, $electroSong]);

// Какой клуб без DJ?

$djKhaled = new Dj($playlist);

// Подвыпивший народ перед заходом в клуб

$people = [
    new Person('Вася', $rapGenre),
    new Person('Петя', $rapGenre),
    new Person('Алиса', $rapGenre),
    new Person('Марина', $rnbGenre),
    new Person('Антон', $rnbGenre),
    new Person('Саша', $rnbGenre),
    new Person('Толик', $electroGenre),
    new Person('Тимур', $electroGenre),
    new Person('Олег', $electroGenre),
];

// Собственно клуб IDOL

$clubIdol = new Club($djKhaled, $people);

// Старт / возобновление вечеринки

$clubIdol->initParty();

// Завершение работы клуба (полное очищение всей текущей активности)

// $clubIdol->endParty();